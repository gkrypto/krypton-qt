# Krypton-Qt

Krypton QT5 Wallet

Krypton-Qt is a free software wallet/front-end for Krypton.

## Usage

Krypton 1.0.0+ is required to be running for Krypton-Qt to work.

Krypton-Qt should auto-detect geth's IPC file/name and work "out of the box" as long as geth is running.

If Krypton-Qt fails to detect the IPC file/name you can specify it in the settings panel.

Do not run Krypton-Qt while geth is syncing, it will just lock down processing all the blocks until syncing is done.

## License

Krypton-Qt is licensed under the GPLv3 license. See LICENSE for more info.

Krypton-Qt is based on Etherwall

## Development

### Requirements

Krypton 1.0.0+

Qt5.2+ with qmake

### Building

qmake -config release && make

