/*
    This file is part of Krypton-Qt based on etherwall.
    etherwall is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    etherwall is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with etherwall. If not, see <http://www.gnu.org/licenses/>.
*/
/** @file AccountsTab.qml
 * @author Krypton team
 * @date 2015
 *
 * Accounts tab
 */
import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.0
Tab {
    id: aboutTab
    title: qsTr("About")

    ColumnLayout {
        id: col
        anchors.margins: 20
        anchors.fill: parent
        RowLayout {
            width: parent.width
            Layout.alignment: Qt.AlignCenter
            Image {
                source: "/images/wallet"
            }
        }
        RowLayout {
            width: parent.width
            Layout.alignment: Qt.AlignCenter
            Label {
                text: qsTr("©2015 Ehterwall")
                font.bold: true
                font.pixelSize: 12
                color: "white"
            }
        }
        RowLayout {
            width: parent.width
            Layout.alignment: Qt.AlignCenter
            Label {
                text: qsTr("©2015 Krypton")
                font.bold: true
                font.pixelSize: 12
                color: "white"
            }
        }
    }
}

